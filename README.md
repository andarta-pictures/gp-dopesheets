# GP Dopesheets

Display and edit Grease pencil frames in the regular Dopesheet


# [Download latest as zip](https://gitlab.com/andarta.pictures/blender/gp-dopesheets/-/archive/main/gp-dopesheets-main.zip)

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list.


# User guide

- Enabling the add-on will add common play/stop and frame range settings in the Dopesheet's header

![guide](docs/gpds_01.png "guide")

- In the top left corner of the dope sheet, click on the ![guide](docs/gpds_02.png "guide") icon to display grease pencil frames

- Once activated, the regular dope sheet has priority over the grease pencil dopesheet (which may seem like frozen). You can now edit those keyframes directly in the regular dope sheet.

![guide](docs/gpds_03.png "guide")

- By clicking on a specific grease pencil channel, Blender will automatically switch active object and layer, allowing you instantly to draw on this specific layer

![guide](docs/gpds_04.png "guide")

- Once you selected a specific channel, you have access to some native layer options (Add/remove, Move Up/Down, Merge down). Isolate layer an Autolock layers functionnality were customized to work in multi objet mode.

![guide](docs/gpds_06.png "guide")

- the lock icon is synced with the layer's lock, but the checkbox will drive the visibility of the layer

![guide](docs/gpds_05.png "guide")

- You can also extend/reduce keyframes exposure with the default keymap F5 / SHIFT F5 (you can set your own shortcuts in the addon's preferences)

# Known Bugs and limitations

- doit ignorer les calques/objets issus des autres add-ons (Onion skin)
- toutes les fonctionnalitées du menu 'layer specials' ne sont pas encore accessibles depuis la dopesheet (ex : move layer to object)
- manque la possibilité d'activer/desactiver l'onion skinning par calque depuis la dopesheet

# Warning

This tool is in development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)




